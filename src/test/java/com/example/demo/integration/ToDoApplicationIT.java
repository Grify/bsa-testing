package com.example.demo.integration;

import com.example.demo.controller.ToDoController;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ToDoController.class)
@Import(ToDoService.class)
public class ToDoApplicationIT {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ToDoRepository repository;

    //No mocks but MockMvc
    @Test
    public void toDoGetAll_ReturnEmpty() throws Exception {
        mockMvc.perform(get("/todos"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void toDoGet_returnResponse() throws Exception {
        when(repository.findById(1L)).thenReturn(Optional.of(new ToDoEntity(1L, "Text")));

        mockMvc.perform(get("/todos/1"))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(1))
                    .andExpect(jsonPath("$.text").value("Text"));
    }

    @Test
    void upsertTodoWhenIdNull_returnResponse() throws Exception {
        when(repository.save(any(ToDoEntity.class))).thenReturn(new ToDoEntity(null, "Text"));

        mockMvc.perform(post("http://localhost/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\": \"null\", \"text\": \"Text\"}")
                .characterEncoding("utf-8"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value("Text"));
    }

    @Test
    void toDoComplete_whenIdNotFound() throws Exception {
        given(repository.findById(1L)).willAnswer(invocationOnMock -> {throw new ToDoNotFoundException(1L);});

        mockMvc.perform(put("http://localhost/todos/1/complete"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("Can not find todo with id 1"));
    }
}

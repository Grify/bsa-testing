package com.example.demo.service;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ToDoServiceTest {
    private ToDoService service;
    private ToDoRepository repository;

    @BeforeEach
    void setUp() {
        this.repository = mock(ToDoRepository.class);
        service = new ToDoService(repository);
    }

    @Test
    public void whenGetOne_thenReturnOne() throws ToDoNotFoundException {
        var obj2 = new ToDoEntity(1L, "The second");

        when(repository.findById(1L)).thenReturn(Optional.of(obj2));

        var toDo = service.getOne(1L);
        assertThat(toDo.text).isEqualTo("The second");
    }

    @Test
    public void whenGetAll_thenReturnAll() {
        var list = new ArrayList<ToDoEntity>();
        var obj1 = new ToDoEntity(0L, "The first");
        var obj2 = new ToDoEntity(1L, "The second");
        var obj3 = new ToDoEntity(2L, "The third");
        list.add(obj1);
        list.add(obj2);
        list.add(obj3);
        when(repository.findAll()).thenReturn(list);

        var response = service.getAll();

        assertEquals(list.size(), response.size());

        for(int i = 0; i < list.size(); i++) {
            assertThat(response.get(i).text).isEqualTo(list.get(i).getText());
        }
    }

    @Test
    public void toDoComplete_whenIdNotFound() {
        assertThrows(ToDoNotFoundException.class, () -> service.completeToDo(0L));
    }

    @Test
    public void whenMapperMap_thenReturnResponse() {
        ToDoResponse response = new ToDoResponse();
        response.id = 0L;
        response.text = "Text";
        ToDoResponse tested = ToDoEntityToResponseMapper.map(new ToDoEntity(0L, "Text"));
        assertThat(response.id).isEqualTo(tested.id);
        assertThat(response.text).isEqualTo(tested.text);
    }
}

package com.example.demo.dto;

import javax.validation.constraints.NotNull;

public class ToDoSaveRequest {
	public Long id;

	@NotNull
	public String text;

	public ToDoSaveRequest(Long id, String text) {
		this.id = id;
		this.text = text;
	}
	public ToDoSaveRequest(String text) {
		this.id = null;
		this.text = text;
	}
}